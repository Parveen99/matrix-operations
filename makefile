LIBDIR:=lib
BUILDDIR:=build
EXEDIR:=bin
EXE:=$(EXEDIR)/matrix
LIB:=$(LIBDIR)/libmatrix.a
BUILD:=$(addprefix $(BUILDDIR)/,$(patsubst src/%.cpp,%.o,$(wildcard src/*.cpp)))
vpath %.cpp src
vpath %.h include

all: $(EXE)
	@echo "Finished successfully"
$(EXE): build/main.o $(LIB) $(LIBDIR)/libmatrix.so |$(EXEDIR)
	@g++ -o $@ -I include/ $< -L $(LIBDIR)/ -lmatrix
	@echo "all ready" 
prepare: scripts/prepare.sh
	@chmod u+x $<
	@./$<
run:scripts/run.sh
	@chmod u+x $<
	@./$<
	
test:scripts/test.sh
	@./$(EXE) -a < tests/sample1.in > tests/output1.out	
	@./$(EXE) -x < tests/sample2.in > tests/output2.out
	@./$(EXE) -a -m -s < tests/sample3.in > tests/output3.out
	@./$(EXE) -s < tests/sample4.in > tests/output4.out
	@chmod u+x $<
	@./$<

	
$(BUILDDIR)/main.o: src/main.cpp include/log.h include/headers.h build/scalar_ops.o build/matrix_ops.o build/matrix_handler.o |
	@g++ -o $@ -c -I include/ $<
$(BUILDDIR)/scalar_ops.o: src/scalar_ops.cpp include/log.h include/headers.h |$(BUILDDIR)
	@g++ -o $@ -c -I include/ $< 
$(BUILDDIR)/matrix_ops.o: src/matrix_ops.cpp include/log.h include/headers.h|
	@g++ -o $@ -fPIC -c -I include/ $<
$(BUILDDIR)/matrix_handler.o: src/matrix_handler.cpp include/log.h include/headers.h |
	@g++ -o $@ -fPIC -c -I include/ $<
$(LIB): build/scalar_ops.o  |$(LIBDIR)
	@ar rcs $@ $^
$(LIBDIR)/libmatrix.so: build/matrix_ops.o build/matrix_handler.o |
	@g++ -shared -o lib/libmatrix.so $< 
	
$(BUILDDIR): 
	@mkdir -p $(BUILDDIR)
	@echo "build/ directory created successfully"
$(EXEDIR):
	@mkdir -p $(EXEDIR)
	@echo "bin/ directory created successfully"
$(LIBDIR):
	@mkdir -p $(LIBDIR)
	@echo "lib/ directory created successfully"
.PHONY:  clean  prepare run build
clean: 
	@rm -rf tests/*.out run.out logfile.log helper_file $(EXEDIR) $(EXEDIR) $(LIBDIR) $(BUILDDIR)
	@echo "cleared"
