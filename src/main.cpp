#include "log.h"
#include "headers.h"
#include "matrix_handler.cpp"
#include "matrix_ops.cpp"
#include "scalar_ops.cpp"
using namespace std;



int main(int argc, char* argv[])
{	
	initLogger( "logfile.log", ldebug);
	vector<int> size; //array to store the dimensions of all the matrices present in input file
	vector<string> file; //vector , that contains the copy of the input file
	int x=size_extractor(size,file); // x will store the count of all the matrices present in the user file
	
	size.pop_back();

		
	int*** arr = new int**[x];  /*this is the key part of the program, this is a 3d array that will store all the matrices of input file and after performing the requested ops, resultant matrix is again stored back into this array with suitable format. at the end of all the ops, results are written to the output file with the help of this array*/
 
    for (int i = 0; i < x; i++) 
    {
 
        // Allocate memory blocks for
        // rows of each 2D array
        arr[i] = new int*[size[2*i]];
 
        for (int j = 0; j < size[2*i]; j++) 
        {
 
            // Allocate memory blocks for
            // columns of each 2D array
            arr[i][j] = new int[size[2*i+1]];
        }
    }


	
	int scalar=file_reader(arr,file);//extracting the scalar value
	
	int option;

	vector<char> ops; //vector that will store the sequence of ops needs to be performed
	while((option=getopt(argc,argv,"asmdtx"))!=-1)
	{	
		ops.push_back(option);

	}
	int choose;
	for(int i=0;i<ops.size();i++)
	{
		choose=ops[i];
		switch(choose)
		{
			case 'a': {if(scalar!=0) add_scalar(arr,scalar,size,x); else add_matrix(arr,&x,size);}break;
			case 's': {if(scalar!=0) sub_scalar(arr,scalar,size,x); else sub_matrix(arr,&x,size);}break;
			case 'm': {if(scalar!=0) mul_scalar(arr,scalar,size,x); else mul_matrix(arr,&x,size,x); }break;
			case 'd': div_scalar(arr,scalar,size,x); break;
			case 't': trans_matrix(arr,&x,size); break;
			case 'x': { scalar=determinant(arr[0],size[0],size);} break;
			
		}

	}
	
	writing_result(arr,size,x,scalar);

  
	
	
		

    return 0;
    
}

