#include "headers.h"
#include"log.h"

using namespace std;
void add_matrix(int ***arr,int *p,vector<int> &size)
{	
	if(size[0]==size[2] && size[1]==size[3])
	{
	int x=*p;
	int temp[size[0]][size[1]]={0};
	for(int i = 0; i < size[0]; ++i)
	{
        	for(int j = 0; j < size[1]; ++j)
            	{
            		temp[i][j] = arr[0][i][j] + arr[1][i][j];
            	}
       }
       for(int i=0;i<x;i++)
       {
       	for(int j=0;j<size[2*i];j++)
       	{	
       		for (int k=0;k<size[2*i+1];k++)
       		{
       			if(i==0) arr[0][j][k]=temp[j][k];
       			else if(i<(x-1))  arr[i][j][k]=arr[i+1][j][k];
       			
       			
       			
       	}	}
       }
       (*p)--;
       size.pop_back();
       size.pop_back();
       }
       else 
       {
       	L_(lerror)<< "matrix addition is not possible, matrices are not compatible for the addition";
       }
       
}
void sub_matrix(int ***arr,int *p,vector<int> &size)
{
	if(size[0]==size[2] && size[1]==size[3])
	{
	int x=*p;
	int temp[size[0]][size[1]]={0};
	for(int i = 0; i < size[0]; ++i)
	{
        	for(int j = 0; j < size[1]; ++j)
            	{
            		temp[i][j] = arr[0][i][j] - arr[1][i][j];
            	}
       }
       for(int i=0;i<x;i++)
       {
       	for(int j=0;j<size[2*i];j++)
       	{	
       		for (int k=0;k<size[2*i+1];k++)
       		{
       			if(i==0) arr[0][j][k]=temp[j][k];
       			else if(i<(x-1))  arr[i][j][k]=arr[i+1][j][k];
       			
       			
       			
       	}	}
       }
       (*p)--;
       size.pop_back();
       size.pop_back();
       }
       else 
       {	
       	L_(lerror)<< "matrix substraction is not possible, matrices are not compatible for the substraction";
       }
       	
}
void mul_matrix(int ***arr,int *p,vector<int> &size,int x)
{
	if(size[1]==size[2] && x!=1)
	{
	int x=*p;
	int temp[size[0]][size[3]];
	
    for (int i = 0; i < size[0]; i++) 
    {
        for (int j = 0; j < size[3]; j++) 
        {
            temp[i][j] = 0;
 
            for (int k = 0; k < size[2]; k++)
             {
                temp[i][j] += arr[0][i][k] * arr[1][k][j];
            }
 
           
        }
 
        
    }

       for(int i=0;i<x;i++)
       {
       	for(int j=0;j<size[2*i];j++)
       	{	
       		for (int k=0;k<size[2*i+1];k++)
       		{
       			if(i==0) arr[0][j][k]=temp[j][k];
       			else if(i<(x-1))  arr[i][j][k]=arr[i+1][j][k];
       			
       			
       			
       	}	}
       }
       (*p)--;
       size.pop_back();
       size.pop_back();
       }
       else 
       {
       	
       	L_(lerror)<< "matrix multiplication is not possible, matrices are not compatible for the multiplication";
       }
        	
}
void trans_matrix(int ***arr,int *p,vector<int> size)
{
	int temp[size[0]][size[1]]={0};
	for (int i = 0; i < size[0]; i++)
        for (int j = 0; j < size[1]; j++)
        {
           { temp[i][j] = arr[0][j][i];}
         }
        for(int i=0;i<size[0];i++)
        {
        	for(int j=0;j<size[1];j++)
        	{
        		arr[0][i][j]=temp[i][j];
        	}
        }  
}
int **submatrix(int **arr, unsigned int n, unsigned int x, unsigned int y) {
    int **submatrix = new int *[n - 1];
    int subi = 0;
    for (int i = 0; i < n; i++) {
        submatrix[subi] = new int[n - 1];
        int subj = 0;
        if (i == y) {
            continue;
        }
        for (int j = 0; j < n; j++) {
            if (j == x) {
                continue;
            }
            submatrix[subi][subj] = arr[i][j];
            subj++;
        }
        subi++;
    }
    return submatrix;
}

int determinant(int **arr, unsigned int n,vector<int>size) {
    if(size[0]==size[2]){int det = 0;
    if (n == 2) {
        return arr[0][0] * arr[1][1] - arr[1][0] *arr[0][1];
    }
    for (int x = 0; x < n; ++x) {
        det += ((x % 2 == 0 ? 1 : -1) * arr[0][x] * determinant(submatrix(arr, n, x, 0), n - 1,size));
    }

    return det;}
    else {L_(lerror)<< "determinant computation is not possible, matrix are not compatible for the determinant computation"; return 0;}
}
